from flask import Flask, jsonify

app = Flask(__name__)

def is_prime(n):
    factor = 0

    for i in range(n):
        if n % (i+1) == 0:
            factor += 1
    
    return factor == 2

def prime_sequence(n):
    counter = 0
    current_number = 2
    result = []
    while counter < int(n):
        if is_prime(current_number):
            counter += 1
            result.append(current_number)
        current_number += 1
    
    return result


@app.route('/')
def home():
    return 'This is Prime Service'

@app.route('/prime/<n>')
def prime_service(n):
    return jsonify(prime_sequence(n))

if __name__ == "__main__":
    app.run(debug=True)